import pytest
from classe import Classe
from student import Student


@pytest.fixture
def classe_students():
    bgl = Classe("Test_logiciel")

    student1 = Student()
    student1.name = "Daniel"
    student1.number = 1

    student2 = Student()
    student2.name = "Alex"
    student2.number = 2

    bgl.saveStudent(student1)
    bgl.saveStudent(student2)

    return bgl



def test_saveStudent():
    bgl = Classe("Test_logiciel")

    student1 = Student()
    student1.name = "Daniel"
    student1.number = 1

    student_list = bgl.saveStudent(student1)

    assert student1 in student_list
    assert len(student_list) == 1


    


def test_is_number_taken(classe_students):
    bgl = classe_students

    student3 = Student()
    student3.name = "Nathan"
    student3.number = 3

    student4 = Student()
    student4.name = "Yade"
    student4.number = 1

    bgl.saveStudent(student3)
    bgl.saveStudent(student4)

    assert bgl.isNumberTaken(2) is True
    assert bgl.isNumberTaken(4) is False

